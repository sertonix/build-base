#!/bin/sh

set -eu

release=${1:-edge}
if [ "$release" != "edge" ]; then
   release="v$release"
fi

echo "https://dl-cdn.alpinelinux.org/alpine/$release/main" > /etc/apk/repositories

extra_pkgs=
gcc_gnat_pkg=$(apk search -x gcc-gnat)

[ -n "$gcc_gnat_pkg" ] && extra_pkgs="$extra_pkgs gcc-gnat"

apk -U upgrade -a

# shellcheck disable=SC2086
apk add --no-cache alpine-sdk lua-aports pigz doas $extra_pkgs

# use buildozer for building
adduser -D buildozer
adduser buildozer abuild
adduser buildozer wheel

# default distfiles location
install -d -g abuild -m 775 /var/cache/distfiles

chmod 0600 /etc/doas.d/doas.conf
